
document.addEventListener('DOMContentLoaded', function() {
  // nav menu
  // const menus = document.querySelectorAll('.side-menu');
  const menus = document.querySelector('.side-menu');
  M.Sidenav.init(menus, {edge: 'right'});
  
  // add recipe form
  // const forms = document.querySelectorAll('.side-form');
  const forms = document.querySelector('.side-form');
  M.Sidenav.init(forms, {edge: 'left'});
});

// add recipe to the list
const recipeList = document.querySelector(".recipes");
function addRecipe(doc) {
  const recipeCard = `
    <div class="card-panel recipe white row" data-id="${doc.id}">
      <img src="./img/dish.png" alt="recipe thumb">
      <div class="recipe-details">
        <div class="recipe-title">${doc.data().title}</div>
        <div class="recipe-ingredients">${doc.data().ingredients}</div>
      </div>
      <div class="recipe-delete material-icons" onclick="onRecipeDelete(event)">
        delete_outline
      </div>
    </div>`;

  recipeList.innerHTML += recipeCard;
}

// remove recipe from the list
function removeRecipe(doc) {
  const cardPanel = document.querySelector(`[data-id="${doc.id}"]`);
  cardPanel.remove();
  // or
  // cardPanel.parentNode.removeChild(cardPanel);
}
