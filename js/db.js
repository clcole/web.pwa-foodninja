
// let firestore enable persistent storage (IndexedDB)
db.enablePersistence()
  .catch(err => {
    // the app is already open in another browser tab
    if(err.code === "failed-precondition") {
      console.log("*** persistence failed ***");
    }

    // the browser is incompatible with the offline persistence implementation
    if(err.code === "unimplemented") {
      console.log("*** persistence not supported ***")
    }
  });

// real-time database listener
db.collection("recipes").onSnapshot(snapshot => {
  snapshot.docChanges().forEach(change => {
    
    if(change.type === "added") {
      addRecipe(change.doc);
    }

    if(change.type === "removed") {
      removeRecipe(change.doc);
    }

  });
});

// add recipe to the db
const addForm = document.querySelector(".add-recipe");
function onReceipeAdd(e) {
  e.preventDefault();

  db.collection("recipes").add({
    title: addForm.title.value,
    ingredients: addForm.ingredients.value
  });

  addForm.title.value = "";
  addForm.title.focus();
  addForm.ingredients.value = "";
}
addForm.addEventListener("submit", (e) => onReceipeAdd(e));

// delete recipe from the db
function onRecipeDelete(e) {
  const id = e.target.parentElement.getAttribute("data-id");
  db.collection("recipes").doc(id).delete();
}
