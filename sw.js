
const staticCache = "site-static-v1";
const dynamicCache = "site-dynamic-v1";
const assets = [
  "/",
  "/index.html",
  "/js/materialize.min.js",
  "/js/app.js",
  "/js/ui.js",
  "/js/db.js",
  "/css/materialize.min.css",
  "/css/styles.css",
  "/img/dish.png",
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://fonts.gstatic.com/s/materialicons/v53/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2",
  "/pages/fallback.html"
];

const limitCacheSize = (name, size) => {
  caches.open(name).then(cache => {
    cache.keys().then(keys => {
      if (keys.length > size) {
        cache.delete(keys[0]).then(limitCacheSize(name, size));
      }
    });
  });
}

self.addEventListener("install", e => {
  console.log("service worker installed");

  e.waitUntil(
    caches.open(staticCache).then(cache => {
      console.log("caching shell assets");
      cache.addAll(assets);
    })
  );

});

self.addEventListener("activate", e => {
  console.log("service worker activated");

  e.waitUntil(
    caches.keys().then(keys => {
      return Promise.all(
        keys.filter(key => key !== staticCache && key !== dynamicCache)
          .map(key => caches.delete(key))
      );
    })
  );
});

self.addEventListener("fetch", e => {
  // don't cache a firestore request
  if (e.request.url.indexOf("firestore.googleapis.com") !== -1) {
    return;
  }

  e.respondWith(
    caches.match(e.request.url)
      .then(cacheRes => {
        return cacheRes || fetch(e.request).then(fetchRes => {
          return caches.open(dynamicCache).then(cache => {
            cache.put(e.request.url, fetchRes.clone()).then(() => limitCacheSize(dynamicCache, 15));
            return fetchRes;
          });
        });
      })
      .catch(() => {
        if (e.request.url.indexOf(".html") !== -1) {
          return caches.match("/pages/fallback.html")
        }
      })
  );
});
